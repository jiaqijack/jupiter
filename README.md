Jupiter
=======

![Jupiter Logo](https://jiaqijack.bitbucket.io/images/jupiter.png)

Jupiter is a collection of Docker images of commonly used data systems:

* [Hadoop](https://bitbucket.org/jiaqijack/jupiter/src/hadoop/)
* [HBase](https://bitbucket.org/jiaqijack/jupiter/src/hbase/)
* [Hadoop Eco System Doc](https://bitbucket.org/jiaqijack/jupiter/src/hadoopdoc/)
* [TensorFlow](https://bitbucket.org/jiaqijack/jupiter/src/tensorflow/)
* [Redis](https://bitbucket.org/jiaqijack/jupiter/src/redis/)
* [Apache Drill](https://bitbucket.org/jiaqijack/jupiter/src/drill/)
* [MongoDB](https://bitbucket.org/jiaqijack/jupiter/src/mongo/)
* [Druid](https://bitbucket.org/jiaqijack/jupiter/src/druid/)
* [OpenTSDB](https://bitbucket.org/jiaqijack/jupiter/src/opentsdb/)
* [Apache ZooKeeper](https://bitbucket.org/jiaqijack/jupiter/src/zookeeper/)
* [Apache Pig](https://bitbucket.org/jiaqijack/jupiter/src/apachepig/)
* [Apache Oozie](https://bitbucket.org/jiaqijack/jupiter/src/tez/) (WIP)
* [Apache Sqoop](https://bitbucket.org/jiaqijack/jupiter/src/sqoop/) (WIP)
* [Nginx](https://bitbucket.org/jiaqijack/jupiter/src/nginx/)
* [Apache Tez](https://bitbucket.org/jiaqijack/jupiter/src/tez/) (WIP)
* [CHEF](https://bitbucket.org/jiaqijack/jupiter/src/chef/)
* [Jenkins](https://bitbucket.org/jiaqijack/jupiter/src/jenkins/)
* [Faban](https://bitbucket.org/jiaqijack/jupiter/src/faban/) (WIP)
